# SimpleServlet
This application based on JavaEE(Servlets). They have two variants of work, with Mock data and with connection to Postgres DB.
Сapabilities.
- Sorting
- Filtering
 
Filter notes:
- Text fields are filtered using “like” criteria.
- Integer fields are filtered using “not less” criteria.
- Date fields are filtered using range (after, before) criteria.

## Build

To build .WAR file, simply run:

    mvn package
