package org.servlet.simple.persistence;

import org.servlet.simple.entity.Part;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

@ApplicationScoped
public class Repository {

    @Inject
    Connection connection;

    public List<Part> findAll(String query){
        List<Part> result = new ArrayList<>();
        if(connection==null)
            return result;

        try (Statement statement = connection.createStatement()){
            ResultSet resultSet = statement.executeQuery(query);
            while(resultSet.next())
                result.add(new Part(resultSet));
        }catch (Exception ex){
            ex.printStackTrace();
        }finally {
            return result;
        }
    }
}
