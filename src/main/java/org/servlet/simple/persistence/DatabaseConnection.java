package org.servlet.simple.persistence;

import org.servlet.simple.support.FileProperties;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import javax.enterprise.context.ApplicationScoped;
import javax.enterprise.inject.Produces;
import javax.inject.Inject;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.Properties;

@ApplicationScoped
public class DatabaseConnection {

    @Inject
    @FileProperties
    Properties properties;

    private Connection connection;

    @PostConstruct
    public void init(){
        try {
            Class.forName(properties.getProperty("driver"));
            connection = DriverManager.getConnection(
                    properties.getProperty("url")
                    , properties.getProperty("user")
                    ,properties.getProperty("password")
            );
        } catch (ClassNotFoundException ex){
            System.out.println("Wrong driver class name");
        } catch (SQLException ex){
            System.out.println("Wrong connection properties");
            ex.printStackTrace();
        } catch (Exception ex){
            ex.printStackTrace();
        }
    }

    @Produces
    public Connection getConnection() {
        return connection;
    }

    @PreDestroy
    public void destroy(){
        if(connection!=null)
            try {
                connection.close();
            }catch (SQLException ex){
                ex.printStackTrace();
            }
    }
}
