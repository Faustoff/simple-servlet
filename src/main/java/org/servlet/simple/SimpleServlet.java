package org.servlet.simple;

import org.servlet.simple.repository.PartDAO;
import org.servlet.simple.util.Spec;

import java.io.IOException;
import java.util.*;
import javax.annotation.PostConstruct;
import javax.enterprise.context.SessionScoped;
import javax.inject.Inject;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *  Servlet for showing result of select part
 */
@SessionScoped
public class SimpleServlet extends HttpServlet {
    private static final long serialVersionUID = 1;

    @Inject
    private PartDAO partDao;

    /**
     *  Key-value store for used filters
     */
    private Map<String,String> filteringMap= new HashMap<>();

    @PostConstruct
    public void start() {
        filteringMap.put("name","");
        filteringMap.put("number","");
        filteringMap.put("vendor","");
        filteringMap.put("qty","");
        filteringMap.put("shipped_since","");
        filteringMap.put("shipped_to","");
        filteringMap.put("received_since","");
        filteringMap.put("received_to","");
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        request.setAttribute("baseUrl",request.getRequestURI());

        Spec spec = getSpecByRequest(request);
        mappingFilters(request);
        spec.setFilters(filteringMap);
        request.setAttribute("filteringMap",filteringMap);
        request.setAttribute("parts",partDao.getAll(spec));
        
        getServletContext().getRequestDispatcher("/part/parts.jsp").forward(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        request.setAttribute("baseUrl",request.getRequestURI());

        Spec spec = getSpecByRequest(request);
        mappingFilters(request);
        spec.setFilters(filteringMap);
        request.setAttribute("filteringMap",filteringMap);
        request.setAttribute("parts",partDao.getAll(spec));

        request.getRequestDispatcher("/part/parts.jsp").forward(request, response);
    }

    @Override
    public void init() throws ServletException {}
    @Override
    public void destroy() {}

    /**
     *  Prepare Spec by selected column for sorting.
     */
    private Spec getSpecByRequest(HttpServletRequest request){
        Spec spec = new Spec();
        String sort = request.getParameter("sort");
        if(sort!=null&&!sort.isEmpty()){
            String type = request.getParameter("by");
            spec.setOrderField(sort).setTypeOrdering(type);
            if("ask".equalsIgnoreCase(type))
                sort = "";
        }else{
            sort = "";
        }
        request.setAttribute("sort",sort);
        return spec;
    }

    /**
     *  Filling map by selected filters
     */
    private void mappingFilters(HttpServletRequest request){
        if(request.getParameter("name")!=null){
            filteringMap.put("name",request.getParameter("name"));
        }
        if(request.getParameter("number")!=null){
            filteringMap.put("number",request.getParameter("number"));
        }
        if(request.getParameter("vendor")!=null){
            filteringMap.put("vendor",request.getParameter("vendor"));
        }
        if(request.getParameter("qty")!=null){
            filteringMap.put("qty",request.getParameter("qty"));
        }
        if(request.getParameter("shipped_since")!=null){
            filteringMap.put("shipped_since",request.getParameter("shipped_since"));
        }
        if(request.getParameter("shipped_to")!=null){
            filteringMap.put("shipped_to",request.getParameter("shipped_to"));
        }
        if(request.getParameter("received_since")!=null){
            filteringMap.put("received_since",request.getParameter("received_since"));
        }
        if(request.getParameter("received_to")!=null){
            filteringMap.put("received_to",request.getParameter("received_to"));
        }
    }
}