package org.servlet.simple.repository;

import org.servlet.simple.entity.Part;
import org.servlet.simple.util.PartComparator;
import org.servlet.simple.util.PartDummeFabric;
import org.servlet.simple.util.Spec;

import javax.annotation.PostConstruct;
import javax.enterprise.context.ApplicationScoped;
import javax.enterprise.inject.Alternative;
import javax.enterprise.inject.Default;
import java.util.ArrayList;
import java.util.List;

/**
 *  Alternative implementation DAO, which get generated objects
 */
@Alternative
@ApplicationScoped
public class PartDaoMockImpl implements PartDAO {

    private List<Part> parts = new ArrayList<>();

    @PostConstruct
    void init(){
        for(int i = 0; i < 20; i++){
            parts.add(PartDummeFabric.getNextPart(i));
        }
    }

    @Override
    public List<Part> getAll(Spec spec) {
        List<Part> result = new ArrayList<>(parts);
        result.sort(new PartComparator(spec));
        return result;
    }
}
