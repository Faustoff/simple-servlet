package org.servlet.simple.repository;

import org.servlet.simple.entity.Part;
import org.servlet.simple.util.Spec;

import java.util.List;

/**
 *   Main interface for access to Part entity
 */
public interface PartDAO {

    List<Part> getAll(Spec spec);

}
