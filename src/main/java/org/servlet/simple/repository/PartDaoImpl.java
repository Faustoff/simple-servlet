package org.servlet.simple.repository;

import org.servlet.simple.entity.Part;
import org.servlet.simple.persistence.Repository;
import org.servlet.simple.util.Spec;

import javax.enterprise.context.ApplicationScoped;
import javax.enterprise.inject.Default;
import javax.inject.Inject;
import javax.persistence.Query;
import java.util.ArrayList;
import java.util.List;


/**
 *  Default implementation DAO, which connected to DB
 */

@Default
@ApplicationScoped
public class PartDaoImpl implements PartDAO {

    @Inject
    private Repository repository;

    @Override
    public List<Part> getAll(Spec spec) {
        try {
            StringBuilder query = new StringBuilder("SELECT * FROM part WHERE 1 = 1 ");
            for(String clause: spec.getFilters().keySet()){
                switch (clause){
                    case "qty":
                        if(!spec.getFilters().get(clause).isEmpty())
                            query.append("AND qty < ").append(spec.getFilters().get(clause)).append(" ");
                        break;
                    case "shipped_since":
                        if(!spec.getFilters().get(clause).isEmpty())
                            query.append("AND shipped > '").append(spec.getFilters().get(clause)).append("' ");
                        break;
                    case "shipped_to":
                        if(!spec.getFilters().get(clause).isEmpty())
                            query.append("AND shipped < '").append(spec.getFilters().get(clause)).append("' ");
                        break;
                    case "received_since":
                        if(!spec.getFilters().get(clause).isEmpty())
                            query.append("AND receive > '").append(spec.getFilters().get(clause)).append("' ");
                        break;
                    case "received_to":
                        if(!spec.getFilters().get(clause).isEmpty())
                            query.append("AND receive < '").append(spec.getFilters().get(clause)).append("' ");
                        break;
                    case "name":
                        if(!spec.getFilters().get(clause).isEmpty())
                            query.append("AND partName LIKE '%").append(spec.getFilters().get(clause)).append("%' ");
                        break;
                    case "number":
                        if(!spec.getFilters().get(clause).isEmpty())
                            query.append("AND partNumber LIKE '%").append(spec.getFilters().get(clause)).append("%' ");
                        break;
                    case "vendor":
                        if(!spec.getFilters().get(clause).isEmpty())
                            query.append("AND vendor LIKE '%").append(spec.getFilters().get(clause)).append("%' ");
                        break;
                }
            }
            query.append("ORDER BY ").append(spec.getOrderField()).append(" ").append(spec.getTypeOrdering());
            return repository.findAll(query.toString());
        }catch (Exception ex){
            System.out.println("DAO exception");
            ex.printStackTrace();
            return new ArrayList<>();
        }
    }
}
