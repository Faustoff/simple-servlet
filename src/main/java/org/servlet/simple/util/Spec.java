package org.servlet.simple.util;

import java.util.HashMap;
import java.util.Map;

/**
 *  Object for transfer condition of Part select
 */
public class Spec {
    private String orderField = "id";
    private String typeOrdering = "asc";

    private Map<String,String> filters = new HashMap<>();


    public String getOrderField() {
        return orderField;
    }

    public Spec setOrderField(String orderField) {
        this.orderField = orderField;
        return this;
    }

    public String getTypeOrdering() {
        return typeOrdering;
    }

    public Spec setTypeOrdering(String typeOrdering) {
        this.typeOrdering = typeOrdering;
        return this;
    }

    public Map<String, String> getFilters() {
        return filters;
    }

    public void setFilters(Map<String, String> filters) {
        this.filters = filters;
    }
}