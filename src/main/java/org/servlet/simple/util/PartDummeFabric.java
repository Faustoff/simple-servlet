package org.servlet.simple.util;

import org.servlet.simple.entity.Part;

import java.util.Date;
import java.util.Random;

/**
 *  Alternative data source for Mock implementation DAO
 */
public class PartDummeFabric {
    public static Part getNextPart(int i){
        Part part = new Part();
        part.setId(i);
        part.setPartName("Part name "+i);
        part.setPartNumber("PN"+i);
        part.setQty(i* new Random().nextInt());
        part.setVendor("Vendor #"+i%3);
        part.setReceive(new Date());
        part.setShipped(new Date());
        return part;
    }
}
