package org.servlet.simple.util;

import org.servlet.simple.entity.Part;

import java.util.Comparator;

/**
 *  Comparator for Part, use spec, simulated "ORDER BY" sql command
 */
public class PartComparator implements Comparator<Part> {
    private Spec spec;

    public PartComparator(Spec spec){
        this.spec = spec;
    }

    @Override
    public int compare(Part o1, Part o2) {
        switch (spec.getOrderField()){
            case "name": return o1.getPartName().compareTo(o2.getPartName())*deskOrAsk(spec);
            case "number": return o1.getPartNumber().compareTo(o2.getPartNumber())*deskOrAsk(spec);
            case "vendor": return o1.getVendor().compareTo(o2.getVendor())*deskOrAsk(spec);
            case "qty": return (o1.getQty()-o2.getQty())*deskOrAsk(spec);
            case "shipped": return o1.getShipped().compareTo(o2.getShipped())*deskOrAsk(spec);
            case "received": return o1.getReceive().compareTo(o2.getReceive())*deskOrAsk(spec);
            default: return (o1.getId() - o2.getId())*deskOrAsk(spec);
        }
    }

    private int deskOrAsk(Spec spec){
        return spec.getTypeOrdering().equalsIgnoreCase("desc")?1:-1;
    }
}
