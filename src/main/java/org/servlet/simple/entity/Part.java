package org.servlet.simple.entity;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Date;
import java.util.Random;

public class Part {
    private int id;
    private String partName;
    private String partNumber;
    private String vendor;
    private int qty;
    private Date shipped;
    private Date receive;

    public Part(){}

    public Part(ResultSet resultSet) throws SQLException{
        id = new Random().nextInt(43);
        this.partName = resultSet.getString("partName");
        this.partNumber = resultSet.getString("partNumber");
        this.vendor = resultSet.getString("vendor");
        this.qty = resultSet.getInt("qty");
        this.shipped = resultSet.getDate("shipped");
        this.receive = resultSet.getDate("receive");
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getPartName() {
        return partName;
    }

    public void setPartName(String partName) {
        this.partName = partName;
    }

    public String getPartNumber() {
        return partNumber;
    }

    public void setPartNumber(String partNumber) {
        this.partNumber = partNumber;
    }

    public String getVendor() {
        return vendor;
    }

    public void setVendor(String vendor) {
        this.vendor = vendor;
    }

    public int getQty() {
        return qty;
    }

    public void setQty(int QTY) {
        this.qty = QTY;
    }

    public Date getShipped() {
        return shipped;
    }

    public void setShipped(Date shipped) {
        this.shipped = shipped;
    }

    public Date getReceive() {
        return receive;
    }

    public void setReceive(Date receive) {
        this.receive = receive;
    }

    @Override
    public boolean equals(Object obj) {
        return obj instanceof Part && ((Part) obj).getId() == this.getId();
    }

    @Override
    public int hashCode() {
        return this.getId();
    }
}
