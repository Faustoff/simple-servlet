<%@ page import="org.servlet.simple.entity.Part" %>
<%@ page import="java.util.ArrayList" %>
<%@ page import="java.text.Format" %>
<%@ page import="java.text.SimpleDateFormat" %>
<%@ page import="java.util.Map" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>
<%
    Format dateFormat = new SimpleDateFormat("MMM dd, yyyy");
%>
<head>
    <title>Title</title>
</head>
<body>
<form method="POST" action="<%=request.getContextPath()%>/parts">
    <%
        Map<String,String> filteredMap = (Map<String,String>) request.getAttribute("filteringMap");
    %>
    <table border="0">
        <tr>
            <td>Name</td>
            <td><input type="text" name="name" value="<%=filteredMap.get("name")%>"/></td>
        </tr>
        <tr>
            <td>Number</td>
            <td><input type="text" name="number" value="<%=filteredMap.get("number")%>"/></td>
        </tr>
        <tr>
            <td>Vendor</td>
            <td><input type="text" name="vendor" value="<%=filteredMap.get("vendor")%>"/></td>
        </tr>
        <tr>
            <td>QTY</td>
            <td><input type="text" name="qty" value="<%=filteredMap.get("qty")%>"/></td>
        </tr>
        <tr>
            <td>Shipped</td>
            <td>Since <input type="date" name="shipped_since" value="<%=filteredMap.get("shipped_since")%>"/>
                to <input type="date" name="shipped_to" value="<%=filteredMap.get("shipped_to")%>"/></td>
        </tr>
        <tr>
            <td>Received</td>
            <td>Since <input type="date" name="received_since" value="<%=filteredMap.get("received_since")%>"/>
                to <input type="date" name="received_to" value="<%=filteredMap.get("received_to")%>"/></td>
        </tr>
        <tr>
            <td colspan="2">
                <input type="submit" value="Refresh"/>
                <a href="<%=request.getContextPath()%>/parts?reset=true">Reset</a>
            </td>
        </tr>
    </table>
</form>
<table>
    <tr>
        <th><a href="<%=request.getContextPath()%>/parts?sort=partName&by=<%
            if(((String)request.getAttribute("sort")).equalsIgnoreCase("partName")){
            %>asc<%}else{
            %>desc"<%}%>">Name</a></th>
        <th><a href="<%=request.getContextPath()%>/parts?sort=partNumber&by=<%
            if(((String)request.getAttribute("sort")).equalsIgnoreCase("partNumber")){
            %>asc<%}else{
            %>desc"<%}%>">Number</a></th>
        <th><a href="<%=request.getContextPath()%>/parts?sort=vendor&by=<%
            if(((String)request.getAttribute("sort")).equalsIgnoreCase("vendor")){
            %>asc<%}else{
            %>desc"<%}%>">Vendor</a></th>
        <th><a href="<%=request.getContextPath()%>/parts?sort=qty&by=<%
            if(((String)request.getAttribute("sort")).equalsIgnoreCase("qty")){
            %>asc<%}else{
            %>desc"<%}%>">QTY</a></th>
        <th><a href="<%=request.getContextPath()%>/parts?sort=shipped&by=<%
            if(((String)request.getAttribute("sort")).equalsIgnoreCase("shipped")){
            %>asc<%}else{
            %>desc"<%}%>">Shipped</a></th>
        <th><a href="<%=request.getContextPath()%>/parts?sort=received&by=<%
            if(((String)request.getAttribute("sort")).equalsIgnoreCase("received")){
            %>asc<%}else{
            %>desc"<%}%>">Received</a></th>
    </tr>
    <%
        ArrayList<Part> posts = (ArrayList<Part>) request.getAttribute("parts");
        for (Part part : posts) {
    %>
    <tr>
        <td><%=part.getPartName()%>
        </td>
        <td><%=part.getPartNumber()%>
        </td>
        <td><%=part.getVendor()%>
        </td>
        <td><%=part.getQty()%>
        </td>
        <td><%=part.getShipped()!=null?dateFormat.format(part.getShipped()):""%>
        </td>
        <td><%=part.getReceive()!=null?dateFormat.format(part.getReceive()):""%>
        </td>
    </tr>
    <%}%>
</table>
</body>
</html>
